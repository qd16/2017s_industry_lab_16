package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

import javax.swing.*;

public class JTableListener extends JTable implements CourseListener{
    @Override
    public void courseHasChanged(Course course) {
        this.updateUI();
    }
}
