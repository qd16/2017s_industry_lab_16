package ictgradschool.industry.lab16.ex02.gui;


import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener {

	private StatisticsPanel _adaptee;

	public StatisticsPanelAdapter(StatisticsPanel panel) {
		_adaptee = panel;
	}

	@Override
	public void courseHasChanged(Course course) {
		_adaptee.updateUI();
	}
}
